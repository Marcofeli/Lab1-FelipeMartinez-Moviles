package com.example.f1419.lab1_felipemartinez;

import android.net.Uri;

/**
 * Created by f1419 on 4/4/2018.
 */

class User {
    private String name;
    private String profession;
    private String sex;
    private Uri imageUri;

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }
}
