package com.example.f1419.lab1_felipemartinez;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    EditText name;
    EditText profession;
    ImageView image;
    Button save;
    RadioGroup sex;
    Button record;
    Button photo;

    private static final int REQUEST_CODE = 1234;
    private static final int IMAGE_CAPTURE = 101;

    List<String> matchesText;

    List<User> users = new ArrayList<>();
    UserAdapter adapter = null;

    Uri imageUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        save = (Button)findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User();
                name = (EditText)findViewById(R.id.name);
                profession = (EditText)findViewById(R.id.profession);
                sex = (RadioGroup)findViewById(R.id.sex);
                user.setName(name.getText().toString());
                user.setProfession(profession.getText().toString());
                switch (sex.getCheckedRadioButtonId()){
                    case R.id.male:
                        user.setSex("Male");
                        break;
                    case R.id.female:
                        user.setSex("Female");
                        break;
                    default:
                        user.setSex("Undefined");
                        break;
                }
                user.setImageUri(imageUri);
                adapter.add(user);
            }
        });
        listView = findViewById(R.id.listView);
        adapter = new UserAdapter();
        listView.setAdapter(adapter);
        record = (Button) findViewById(R.id.record);
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isConnected()){
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    startActivityForResult(intent, REQUEST_CODE);
                } else {
                    Toast.makeText(MainActivity.this, "Cannot recognized anything, ¡Speak louder!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        photo = (Button)findViewById(R.id.photo);
        if (! hasCamera () )
            photo.setEnabled( false ) ;
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent ( MediaStore . ACTION_IMAGE_CAPTURE ) ;
                startActivityForResult ( intent , IMAGE_CAPTURE) ;
            }
        });
    }

    private boolean hasCamera () {
        return ( getPackageManager () . hasSystemFeature (
                PackageManager. FEATURE_CAMERA_ANY ) ) ;
    }

    public class UserAdapter extends ArrayAdapter<User> {

        MainActivity.RowHolder holder = null;

        public UserAdapter() {
            super(MainActivity.this, R.layout.row, users);
        }

        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View row=convertView;
            RowHolder holder=null;
            if(row==null){
                LayoutInflater inflater = getLayoutInflater();
                row=inflater.inflate(R.layout.row, parent,false);
                holder=new RowHolder(row);
                row.setTag(holder);
            }
            else{
                holder=(RowHolder) row.getTag();
            }
            holder.fillLayout(users.get(position));
            return (row);
        }
    }


    static class RowHolder{
        private TextView name,profession,sex;
        private ImageView photo;

        public RowHolder(View row) {
            name = (TextView)row.findViewById(R.id.name);
            profession = (TextView)row.findViewById(R.id.profession);
            sex = (TextView)row.findViewById(R.id.sex);
            photo = (ImageView)row.findViewById(R.id.image);
        }

        void fillLayout(User user){
            name.setText(user.getName());
            profession.setText(user.getProfession());
            sex.setText(user.getSex());
            photo.setImageURI(user.getImageUri());
        }
    }

    public boolean isConnected(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net!= null && net.isAvailable() && net.isConnected()){
            return true;
        }   else {
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            matchesText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            profession.setText(matchesText.get(0));
            return;
        }
        if (requestCode == IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                imageUri = data.getData();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, " Image capture cancelled .",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, " Failed to capture image",
                        Toast.LENGTH_LONG).show();
            }
            return;
        }
            Toast.makeText(MainActivity.this, "Something go wrong, ¡keep trying!", Toast.LENGTH_SHORT).show();
    }

}
